package net.gumcode.malangvoice.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.gumcode.malangvoice.R;
import net.gumcode.malangvoice.activity.DetailActivity;
import net.gumcode.malangvoice.config.Config;
import net.gumcode.malangvoice.model.Post;
import net.gumcode.malangvoice.utilities.Utilities;

import java.util.List;

/**
 * Created by firdausdwika on 8/9/2017.
 */

public class SliderAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<Post> dataPostList;

    public SliderAdapter(Context context, List<Post> dataPostList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dataPostList = dataPostList;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        ViewHolders holder = new ViewHolders(itemView);
        holder.judul.setText(this.dataPostList.get(position).getTitle());
        holder.kategori.setText(this.dataPostList.get(position).getCategory());
        holder.tanggal.setText(Utilities.getDateTime(this.dataPostList.get(position).getTime())+"    -    "+this.dataPostList.get(position).getAuthor());
        Picasso.with(mContext)
                .load(this.dataPostList.get(position).getImgUrl())
                .into(holder.gambar);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle send = new Bundle();
                send.putInt("ID", dataPostList.get(position).getId());
                String kategori = dataPostList.get(position).getCategory();
                send.putString("Category", kategori);
                Intent change = new Intent(mContext, DetailActivity.class);
                change.putExtras(send);
                mContext.startActivity(change);
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private class ViewHolders {

        ImageView gambar;
        TextView judul;
        TextView kategori;
        TextView tanggal;

        public ViewHolders(View rootView) {
            gambar = (ImageView)rootView.findViewById(R.id.gambar_pager);
            judul = (TextView)rootView.findViewById(R.id.judul_pager);
            kategori = (TextView)rootView.findViewById(R.id.category_pager);
            tanggal = (TextView)rootView.findViewById(R.id.tanggal_pager);
        }
    }
}
