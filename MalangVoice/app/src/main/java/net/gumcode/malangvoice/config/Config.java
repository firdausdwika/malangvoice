package net.gumcode.malangvoice.config;

/**
 * Created by A. Fauzi Harismawan on 12/30/2015.
 */
public class Config {

    public static final String ADS_JSON_URL = "http://malangvoice.com/app/api/malangvo/ads/";
    public static final String ALL_POST_JSON_URL = "http://malangvoice.com/wp-json/posts?filter[posts_per_page]=30";
    public static final String BASE_URL = "http://malangvoice.com/app/admin/";
    public static final String CATEGORY_POST_JSON_URL = "http://malangvoice.com/wp-json/posts?filter[posts_per_page]=30&filter[category_name]=";
    public static final String ONE_PAGE_JSON_URL = "http://malangvoice.com/wp-json/pages/";
    //public static final String ONE_POST_JSON_URL = "http://malangvoice.com/wp-json/posts?filter[posts_per_page]=50";
    public static final String ONE_POST_JSON_URL = "http://malangvoice.com/wp-json/posts/";
    public static final String REGISTER_GCM_URL = "http://malangvoice.com/pnfw/register/";
    public static final String REG_ADS_URL = "http://malangvoice.com/iklan";
    public static final String REG_ADS_IKL_URL = "http://malangvoice.com/iklan";
    public static final String REG_ADS_TK_URL = "http://malangvoice.com/tentang-kami";
    public static final String REG_ADS_RDK_URL = "http://malangvoice.com/redaksi";
    public static final String REG_ADS_PPS_URL = "http://malangvoice.com/pedoman-penulisan-siber";
    public static final String REG_ADS_DSC_URL = "http://malangvoice.com/disclaimer";
    public static final String REG_ADS_KEJ_URL = "http://malangvoice.com/kode-etik-jurnalistik";
    public static final String UNREGISTER_GCM_URL = "http://malangvoice.com/pnfw/unregister/";

    //public static final String ALL_POST_JSON_URL = "http://malangvoice.com/wp-json/posts?filter[posts_per_page]=50";
    //public static final String ONE_POST_JSON_URL = "http://malangvoice.com/wp-json/posts/";
    //public static final String CATEGORY_POST_JSON_URL = "http://malangvoice.com/wp-json/posts?filter[posts_per_page]=50&filter[category_name]=";
    // public static final String ADS_JSON_URL = "http://api.harismawan.xyz/malangvo/ads";
    //public static final String ADS_JSON_URL = "http://malangvoice.com/app/api/malangvo/ads/";
    //public static final String REG_ADS_URL = "http://malangvoice.com/iklan";
    //public static final String BASE_URL = "http://malangvoice.com/app/admin/";

    //public static final String ONE_PAGE_JSON_URL = "http://malangvoice.com/wp-json/pages/";
}