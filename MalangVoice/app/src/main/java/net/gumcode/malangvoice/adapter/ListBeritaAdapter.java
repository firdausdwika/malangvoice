package net.gumcode.malangvoice.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import net.gumcode.malangvoice.R;
import net.gumcode.malangvoice.activity.DetailActivity;
import net.gumcode.malangvoice.config.Config;
import net.gumcode.malangvoice.model.Post;
import net.gumcode.malangvoice.utilities.HeightWrappingViewPager;
import net.gumcode.malangvoice.utilities.Utilities;

import java.util.HashMap;
import java.util.List;

/**
 * Created by firdausdwika on 8/10/2017.
 */

public class ListBeritaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Post> postList;
    private Context mContext;
    public final static int VIEW_TYPE_PAGER = 0;
    public static final int VIEW_TYPE_TEXT = 1;
    public HashMap<Integer, Integer> mViewPageStates = new HashMap<>();

    public ListBeritaAdapter(List<Post> postList, Context context){
        this.postList = postList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        if (viewType == VIEW_TYPE_PAGER){
            View blockbusterView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_pager, parent, false);
            return new MyViewHolderPager(blockbusterView);
        } else if(viewType >=1 && viewType<=3){
            View data = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rss_feeds,parent,false);
            return new MyViewHolder(data);
        } else if((viewType+1) % 5 == 0){
            View data = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ads,parent,false);
            data.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(postList.get(viewType).getAuthor()));
                    mContext.startActivity(browserIntent);
                }
            });
            return new MyViewAds(data);
        } else{
            View userView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rss_feeds, parent, false);
            userView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle send = new Bundle();
                    send.putInt("ID", postList.get(viewType).getId());
                    String kategori = postList.get(viewType).getCategory();
                    send.putString("Category", kategori);
                    Intent change = new Intent(mContext, DetailActivity.class);
                    change.putExtras(send);
                    mContext.startActivity(change);
                }
            });
            return new MyViewHolder(userView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int itemType = getItemViewType(position);

        if (itemType == VIEW_TYPE_PAGER) {
            MyViewHolderPager pagerHolder = (MyViewHolderPager) holder;
            configurePagerHolder(pagerHolder, position);
        }
        else if(itemType >=1 && itemType <=3){
            MyViewHolder textHolder = (MyViewHolder) holder;
            configureHidden(textHolder,position);
        }else if((itemType + 1) %5 == 0){
            MyViewAds textHolder = (MyViewAds) holder;
            configureAds(textHolder,position);
        }
        else{
            MyViewHolder textHolder = (MyViewHolder) holder;
            configureTextItem(textHolder, position);
        }
    }

    private void configureTextItem(MyViewHolder holder, int position) {
        Post posts = postList.get(position);
        holder.title.setText(posts.getTitle());
        holder.time.setText(Utilities.getStringTime(posts.getTime()));
        Picasso.with(holder.img.getContext()).load(posts.getImgUrl()).into(holder.img);
    }

    private void configureAds(MyViewAds holder, int position) {
        Post posts = postList.get(position);
        Log.e("GAMBAR", posts.getImgUrl());
        if(posts.getImgUrl().substring(posts.getImgUrl().lastIndexOf(".")).equalsIgnoreCase(".jpg") || posts.getImgUrl().substring(posts.getImgUrl().lastIndexOf(".")).equalsIgnoreCase(".jpeg")){
            Picasso.with(holder.img.getContext()).load(posts.getImgUrl()).into(holder.img);
        }else{
            Glide.with(holder.img.getContext()).load(posts.getImgUrl()).placeholder(R.mipmap.ic_launcher).dontAnimate().into(holder.img);
            //Picasso.with(holder.img.getContext()).load(posts.getImgUrl()).into(holder.img);
        }
    }

    private void configureHidden(MyViewHolder holder, int position) {
//        holder.linearLayout.setVisibility(View.INVISIBLE);
        holder.linearLayout.removeAllViews();
    }

    private void configurePagerHolder(MyViewHolderPager holder, int position) {
        SliderAdapter adapter = new SliderAdapter(mContext, postList);
        holder.viewPager.setAdapter(adapter);
        holder.viewPager.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (mViewPageStates.containsKey(position)) {
            holder.viewPager.setCurrentItem(mViewPageStates.get(position));
        }
    }

    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof MyViewHolderPager) {
            MyViewHolderPager viewHolder = (MyViewHolderPager) holder;
            mViewPageStates.put(holder.getAdapterPosition(), viewHolder.viewPager.getCurrentItem());
            super.onViewRecycled(holder);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, time;
        public ImageView img;
        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            linearLayout = (LinearLayout) view.findViewById(R.id.linear_rss);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            img = (ImageView) view.findViewById(R.id.img);
            //Picasso.with(getContext()).load(Config.BASE_URL + w.getImgUrl()).into(img);
        }
    }

    public class MyViewAds extends RecyclerView.ViewHolder {
        public ImageView img;

        public MyViewAds(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.img);
            //Picasso.with(getContext()).load(Config.BASE_URL + w.getImgUrl()).into(img);
        }
    }

    public class MyViewHolderPager extends RecyclerView.ViewHolder {
        public TextView judul, kategori, tanggal;
        public ImageView gambar;
        public HeightWrappingViewPager viewPager;

        public MyViewHolderPager(View view) {
            super(view);
            gambar = (ImageView)view.findViewById(R.id.gambar_pager);
            judul = (TextView)view.findViewById(R.id.judul_pager);
            kategori = (TextView)view.findViewById(R.id.category_pager);
            tanggal = (TextView)view.findViewById(R.id.tanggal_pager);
            viewPager = (HeightWrappingViewPager) view.findViewById(R.id.pager);
            //Picasso.with(getContext()).load(Config.BASE_URL + w.getImgUrl()).into(img);
        }
    }
}
