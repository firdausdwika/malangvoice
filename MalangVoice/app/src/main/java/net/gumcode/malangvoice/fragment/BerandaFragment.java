package net.gumcode.malangvoice.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import net.gumcode.malangvoice.R;
import net.gumcode.malangvoice.activity.DetailActivity;
import net.gumcode.malangvoice.adapter.ListBeritaAdapter;
import net.gumcode.malangvoice.adapter.NewsListAdapter;
import net.gumcode.malangvoice.adapter.SliderAdapter;
import net.gumcode.malangvoice.config.Config;
import net.gumcode.malangvoice.config.Constants;
import net.gumcode.malangvoice.model.Post;
import net.gumcode.malangvoice.utilities.HTTPHelper;
import net.gumcode.malangvoice.utilities.HeightWrappingViewPager;
import net.gumcode.malangvoice.utilities.JSONParser;
import net.gumcode.malangvoice.utilities.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firdausdwika on 8/9/2017.
 */

public class BerandaFragment extends Fragment{

    //private ListView list;
    private RecyclerView recyclerView;
    private List<Post> postList;
    private List<Post> adsList;
    private List<Post> fixedList = new ArrayList<>();
    //private SwipeRefreshLayout swipe;

    //private HeightWrappingViewPager pager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_beranda, container, false);
        //list = (ListView) rootView.findViewById(R.id.list);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.list_berita);
        //swipe = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        //pager = (HeightWrappingViewPager) rootView.findViewById(R.id.pager);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Bundle recv = getArguments();
        if (recv == null) {
            getData(Config.ALL_POST_JSON_URL);
            //getData(Config.ONE_POST_JSON_URL);
        } else {
            getData(Config.CATEGORY_POST_JSON_URL + recv.getString("category"));
        }

//        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (recv == null) {
//                    refresh(Config.ALL_POST_JSON_URL);
//                    //refresh(Config.ONE_POST_JSON_URL);
//                } else {
//                    refresh(Config.CATEGORY_POST_JSON_URL + recv.getString("category"));
//                }
//            }
//        });
    }

    private void initList() {
//        SliderAdapter sliderAdapter = new SliderAdapter(getActivity(), postList);
//        pager.setAdapter(sliderAdapter);
//        pager.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //NewsListAdapter adapter = new NewsListAdapter(getActivity(), R.layout.list_item_rss_feeds, fixedList);
        //list.setAdapter(adapter);
        //list.setOnItemClickListener(this);
        //Log.e("FIXED LIST", String.valueOf(fixedList.get(4).getAuthor()));
        ListBeritaAdapter listBeritaAdapter = new ListBeritaAdapter(fixedList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false));
        recyclerView.setHasFixedSize(true);
        //recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(listBeritaAdapter);
    }

//    private void refresh(final String url) {
//        new AsyncTask<Void, Void, Void>() {
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                Log.d(Constants.LOG_TAG, url);
//                postList = JSONParser.parsePostList(HTTPHelper.sendGETRequest(url));
//                adsList = JSONParser.parseAdsList(HTTPHelper.sendGETRequest(Config.ADS_JSON_URL));
//                fixedList.clear();
//                int i = 1;
//                int j = 0;
//                int k = 0;
//                boolean run = true;
//                while (run) {
//                    if (i % 4 == 0) {
//                        if (j == adsList.size()) {
//                            j = 0;
//                        }
//                        if (adsList.size() != 0) {
//                            fixedList.add(adsList.get(j));
//                            j++;
//                        }
//                    } else {
//                        if (postList.size() != 0) {
//                            fixedList.add(postList.get(k));
//                            k++;
//                        }
//                        if (k == postList.size()) {
//                            run = false;
//                        }
//                    }
//                    i++;
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//                super.onPostExecute(aVoid);
//                if (postList.size() != 0) {
//                    initList();
//                } else {
//                    // no post
//                }
//                swipe.setRefreshing(false);
//            }
//        }.execute();
//    }

    private void getData(final String url) {
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog pd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Loading...");
                pd.setIndeterminate(true);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Log.d(Constants.LOG_TAG, url);
                postList = JSONParser.parsePostList(HTTPHelper.sendGETRequest(url));
                adsList = JSONParser.parseAdsList(HTTPHelper.sendGETRequest(Config.ADS_JSON_URL));
                fixedList.clear();
                int i = 1;
                int j = 0;
                int k = 0;
                boolean run = true;
                while (run) {
                    if (i % 5 == 0) {
                        if (j == adsList.size()) {
                            j = 0;
                        }
                        if (adsList.size() != 0) {
                            fixedList.add(adsList.get(j));
                            j++;
                        }
                    } else {
                        if (postList.size() != 0) {
                            fixedList.add(postList.get(k));
                            k++;
                        }
                        if (k == postList.size()) {
                            run = false;
                        }
                    }
                    i++;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (postList.size() != 0) {
                    initList();
                } else {
                    // no post
                }
                pd.dismiss();
            }
        }.execute();
    }
}
